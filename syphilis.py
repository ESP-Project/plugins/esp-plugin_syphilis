"""
                                  ESP Health
                         Notifiable Diseases Framework
                           Syphilis Case Generator


@author: Bob Zambarano <bzambarano@commoninf.com>
@organization: Commonwealth Informatics http://www.commoninf.com
@contact: http://www.esphealth.org
@copyright: (c) 2017 Commonwealth Informatics
@license: LGPL
"""

import math
from ESP.conf.models import ConditionConfig
from ESP.conf.models import LabTestMap
from ESP.hef.base import DiagnosisHeuristic
from ESP.hef.base import Dose
from ESP.hef.base import Dx_CodeQuery
from ESP.hef.base import Event
from ESP.hef.base import LabResultPositiveHeuristic
from ESP.hef.base import PrescriptionHeuristic
from ESP.nodis.base import DiseaseDefinition
from ESP.nodis.models import Case
from ESP.settings import FILTER_CENTERS
from ESP.conf.models import ConditionConfig, LabTestMap
from ESP.static.models import DrugSynonym
from ESP.utils import log
# In most instances it is preferable to use relativedelta for date math.
# However when date math must be included inside an ORM query, and thus will
# be converted into SQL, only timedelta is supported.
from datetime import timedelta
from dateutil.relativedelta import relativedelta
from django.db.models import F, Q


class Syphilis(DiseaseDefinition):
    """
    Syphilis
    """

    conditions = ['syphilis']

    uri = 'urn:x-esphealth:disease:channing:syphilis:v1'

    short_name = 'syphilis'

    recurrence_interval = 180
    precurrence = -30

    test_name_search_strings = [
        'syph',
        'rpr',
        'vdrl',
        'tp',
        'fta',
        'trep',
        'pallidum',
        'rapid plasma reagin',
        'agglutination',
    ]

    timespan_heuristics = []

    @property
    def event_heuristics(self):
        heuristic_list = []
        #
        # Diagnosis Codes
        #
        heuristic_list.append(DiagnosisHeuristic(
            name='syphilis',
            dx_code_queries=[
                Dx_CodeQuery(starts_with='090', type='icd9'),
                Dx_CodeQuery(starts_with='091', type='icd9'),
                Dx_CodeQuery(starts_with='092', type='icd9'),
                Dx_CodeQuery(starts_with='093', type='icd9'),
                Dx_CodeQuery(starts_with='094', type='icd9'),
                Dx_CodeQuery(starts_with='095', type='icd9'),
                Dx_CodeQuery(starts_with='096', type='icd9'),
                Dx_CodeQuery(starts_with='097', type='icd9'),
                Dx_CodeQuery(starts_with='A50', type='icd10'),
                Dx_CodeQuery(starts_with='A51', type='icd10'),
                Dx_CodeQuery(starts_with='A52', type='icd10'),
                Dx_CodeQuery(starts_with='A53', type='icd10'),
            ]
        ))
        #
        # Prescriptions
        #
        heuristic_list.append(PrescriptionHeuristic(
            name='penicillin_g',
            drugs=DrugSynonym.generics_plus_synonyms(['penicillin g', 'pen g']),
        ))
        heuristic_list.append(PrescriptionHeuristic(
            name='doxycycline_7_day',
            drugs=DrugSynonym.generics_plus_synonyms(['doxycycline', ]),
            min_quantity=14,  # Need 14 pills for 7 days
        ))
        heuristic_list.append(PrescriptionHeuristic(
            name='ceftriaxone_1_or_2_gram',
            drugs=DrugSynonym.generics_plus_synonyms(['ceftriaxone', ]),
            doses=[
                Dose(quantity=1, units='g'),
                Dose(quantity=2, units='g'),
            ],
        ))
        #
        # Lab Results
        #
        heuristic_list.append(LabResultPositiveHeuristic(
            test_name='rpr',
            titer_dilution=1,
        ))
        heuristic_list.append(LabResultPositiveHeuristic(
            test_name='vdrl',
            titer_dilution=1,
        ))
        heuristic_list.append(LabResultPositiveHeuristic(
            test_name='tppa',
        ))
        heuristic_list.append(LabResultPositiveHeuristic(
            test_name='fta-abs',
        ))
        heuristic_list.append(LabResultPositiveHeuristic(
            test_name='tp-igg',
        ))
        heuristic_list.append(LabResultPositiveHeuristic(
            test_name='tp-cia',
        ))
        # new heuristic in v7 of spec
        heuristic_list.append(LabResultPositiveHeuristic(
            test_name='tp-igm',
        ))
        heuristic_list.append(LabResultPositiveHeuristic(
            test_name='vdrl-csf',
            titer_dilution=1,
        ))
        heuristic_list.append(LabResultPositiveHeuristic(
            test_name='tppa-csf',
        ))
        heuristic_list.append(LabResultPositiveHeuristic(
            test_name='fta-abs-csf',
        ))

        return heuristic_list

    def attach_events_to_existing_cases(self, all_ev_names):
        event_qs = Event.objects.filter(name__in=all_ev_names)
        # events already attached to cases
        case_evnt_pks = set(Case.objects.filter(condition=self.conditions[0]).values_list('events__pk', flat=True))
        # new events (not already attached)
        new_evnt_pks = set(event_qs.values_list('pk', flat=True)) - case_evnt_pks
        new_evnt_qs = Event.objects.filter(pk__in=new_evnt_pks).values('patient', 'pk', 'date')
        # build a dictionary of patients and events
        event_lists_dict = {}
        for event in new_evnt_qs:
            try:
                event_lists_dict[event['patient']].append((event['pk'], event['date']))
            except KeyError:
                event_lists_dict[event['patient']] = [(event['pk'], event['date'])]
        event_pats = set(event_lists_dict.keys())
        # build a dictionary of patients and cases
        case_lists_qs = Case.objects.filter(condition=self.conditions[0]).values('patient', 'pk', 'date')
        case_lists_dict = {}
        for case in case_lists_qs:
            try:
                case_lists_dict[case['patient']].append((case['pk'], case['date']))
            except KeyError:
                case_lists_dict[case['patient']] = [(case['pk'], case['date'])]
        case_pats = set(case_lists_dict.keys())
        # now go through the events, 1 patient at a time
        for pat in event_pats:
            # sort events by date
            pat_events = event_lists_dict[pat]
            if len(pat_events) > 1:
                pat_events.sort(key=lambda x: x[1])
            for event in pat_events:
                # check to see if event can be attached to an existing case:
                try:
                    case_list = case_lists_dict[pat]
                    if len(case_list) > 1:
                        case_list.sort(key=lambda x: x[1])
                    for case in case_list:
                        if self.precurrence < (event[1] - case[1]).days < self.recurrence_interval:
                            case_obj = Case.objects.get(pk=case[0])
                            case_obj.events.add(event[0])
                            case_obj.save()
                            # add the event to the case events set
                            case_evnt_pks.add(event[0])
                            break
                except KeyError:
                    # no cases for this patient, so pass
                    pass
        return case_evnt_pks

    def make_querysets(self, evnt_name_list_dict, case_evnt_pks):
        qs_dict = {}
        for key, evnt_name_list in evnt_name_list_dict.items():
            ev_pks = set(Event.objects.filter(name__in=evnt_name_list).values_list('pk', flat=True)) - case_evnt_pks
            qs_dict[key] = Event.objects.filter(pk__in=ev_pks).values('pk', 'patient', 'date')
        return qs_dict

    def make_cases(self, qs_list, crit_list, oth_events):
        event_lists_dict = {}
        critno = 0
        for qs in qs_list:
            for event in qs:
                try:
                    event_lists_dict[event['patient']].append((event['pk'], event['date'], event['provider'], critno))
                except KeyError:
                    event_lists_dict[event['patient']] = [(event['pk'], event['date'], event['provider'], critno)]
            critno += 1
        event_pats = set(event_lists_dict.keys())
        counter = 0
        for pat in event_pats:
            if (FILTER_CENTERS[0] == '' or (FILTER_CENTERS is not None and
                                            Patient.objects.get(pk=pat).center_id in FILTER_CENTERS)):
                status = ConditionConfig.objects.get(name=self.conditions[0]).initial_status
            else:
                status = 'AR'
            case_date = None
            event_list = event_lists_dict[pat]
            if len(event_list) > 1:
                event_list.sort(key=lambda x: x[1])
            else:
                log.info('Less than 2 events for patient %s' % pat)
            for event in event_list:
                if (not case_date) or ((event[1] - case_date).days > self.recurrence_interval):
                    case_date = event[1]
                    new_case = Case(patient_id=pat,
                                    provider_id=event[2],
                                    date=case_date,
                                    condition=self.conditions[0],
                                    criteria=crit_list[event[3]],
                                    source=self.uri,
                                    status=status)
                    new_case.save()
                    counter += 1
                    if event[3] == 1:  # criteria 2, so need to add other events
                        othlx_evnts = oth_events[pat]
                        for othlx_evnt in othlx_evnts:
                            if (event[1] - othlx_evnt[1]).days <= 30:
                                new_case.events.add(othlx_evnt[0])
                                new_case.save()
                new_case.events.add(event[0])
                new_case.save()

        log.debug('Generated %s new cases of syphilis' % counter)
        return counter  # Count of new cases

    def generate(self):
        LabTestMap.create_update_dummy_lab(self.conditions[0], 'MDPH-283', 'MDPH-R444')
        log.info('Generating cases of %s' % self.short_name)
        all_ev_names = ['dx:syphilis',
                        'lx:tp-igm:positive', 'lx:rpr:positive', 'lx:vdrl:positive', 'lx:tppa:positive',
                        'lx:fta-abs:positive', 'lx:tp-igg:positive', 'lx:tp-cia:positive', 'lx:vdrl-csf:positive',
                        'lx:fta-abs-csf:positive', 'lx:tppa-csf:positive',
                        'rx:penicillin_g', 'rx:doxycycline_7_day', 'rx:ceftriaxone_1_or_2_gram']

        case_evnt_pks = self.attach_events_to_existing_cases(all_ev_names)
        #
        # Criteria Set #1
        #
        crit1_evnames_dict = {'dxs': ['dx:syphilis'],
                              'lxs': ['lx:tp-igm:positive'],
                              'rxs': ['rx:penicillin_g', 'rx:doxycycline_7_day', 'rx:ceftriaxone_1_or_2_gram']}
        crit1_qs_dict = self.make_querysets(crit1_evnames_dict, case_evnt_pks)
        # make_querysets will return a dictionary of querysets with the same keys as provided for the list of event names.
        crit1_rx_evnts_dict = {}
        for event in crit1_qs_dict['rxs']:
            try:
                crit1_rx_evnts_dict[event['patient']].append((event['pk'], event['date']))
            except KeyError:
                crit1_rx_evnts_dict[event['patient']] = [(event['pk'], event['date'])]
        crit1_pks = set()
        for evnt in (crit1_qs_dict['dxs'] | crit1_qs_dict['lxs']):
            try:
                rx_evnts = crit1_rx_evnts_dict[evnt['patient']]
                for rx_evnt in rx_evnts:
                    if abs((rx_evnt[1] - evnt['date']).days) <= 14:
                        crit1_pks.add(rx_evnt[0])
                        crit1_pks.add(evnt['pk'])
            except KeyError:
                # no rx events for this dx/lx
                pass
        crit1_qs = Event.objects.filter(pk__in=crit1_pks).values('patient', 'pk', 'date', 'provider')
        qs_list = [crit1_qs]
        crit_list = [
            'Criteria #1: syphilis diagnosis or tp-igm:positive and prescription of penicillin_g or doxycycline_7_day or ceftriaxone_1_or_2_gram w/in 14 days']
        #
        # Criteria Set #2
        #
        crit2_evnames_dict = {'titer_lxs': ['lx:rpr:positive', 'lx:vdrl:positive']}
        crit2_qs_dict = self.make_querysets(crit2_evnames_dict, case_evnt_pks)
        # per criteria 2, we're looking for the following positive events to have occurred "ever in the past", so we don't filter out events attached to existing cases
        crit2_qs_dict['other_lxs'] = Event.objects.filter(
            name__in=['lx:tppa:positive', 'lx:fta-abs:positive', 'lx:tp-igg:positive', 'lx:tp-cia:positive']).values('pk', 'patient', 'date')
        crit2_lx_evnts_dict = {}
        for event in crit2_qs_dict['other_lxs']:
            try:
                crit2_lx_evnts_dict[event['patient']].append((event['pk'], event['date']))
            except KeyError:
                crit2_lx_evnts_dict[event['patient']] = [(event['pk'], event['date'])]
        crit2_pks = set()
        for evnt in crit2_qs_dict['titer_lxs']:
            try:
                othlx_evnts = crit2_lx_evnts_dict[evnt['patient']]
                for othlx_evnt in othlx_evnts:
                    if (evnt['date'] - othlx_evnt[1]).days >= -30:
                        # we don't add the othlx_evnts because they can be "ever in the past" so we don't want them in the set of events that determine case date
                        crit2_pks.add(evnt['pk'])
            except KeyError:
                # no othlx events for this titerlx
                pass
        crit2_qs = Event.objects.filter(pk__in=crit2_pks).values('patient', 'pk', 'date', 'provider')
        qs_list.append(crit2_qs)
        crit_list.append(
            ' Criteria #2: rpr_pos or vdrl_pos and (tppa_pos or fta-abs_pos or tp-igg_pos or tp-cia_pos) ever in the past up to 1 month following positive rpr (titer dilution 1) or vdrl (titer dilution 1)')
        #
        # Criteria Set #3
        #
        crit3_evnames_dict = {'csf': ['lx:vdrl-csf:positive', 'lx:fta-abs-csf:positive', 'lx:tppa-csf:positive']}
        crit3_qs_dict = self.make_querysets(crit3_evnames_dict, case_evnt_pks)
        crit3_pks = set()
        for evnt in crit3_qs_dict['csf']:
            crit3_pks.add(evnt['pk'])
        crit3_qs = Event.objects.filter(pk__in=crit3_pks).values('patient', 'pk', 'date', 'provider')
        qs_list.append(crit3_qs)
        crit_list.append('Criteria #3: vdrl-csf:pos (titer dilution 1) or  fta-abs-csf:pos or tppa-csf:pos')

        new_case_count = self.make_cases(qs_list, crit_list, crit2_lx_evnts_dict)
        self.attach_events_to_existing_cases(all_ev_names) #Attach still unbound events

        log.debug('Generated %s new cases of syphilis' % new_case_count)

        return new_case_count

    def report_field(self, report_field, case):
        reportable_fields = {
            'na_trmt_obx': False,
            'symptom_obx': False,
            'NA-56': 'NA-1739',
            '10187-3': 'NA-1738',
        }

        return reportable_fields.get(report_field, None)

# -------------------------------------------------------------------------------
#
# Packaging
#
# -------------------------------------------------------------------------------

syphilis_definition = Syphilis()


def event_heuristics():
    return syphilis_definition.event_heuristics


def disease_definitions():
    return [syphilis_definition]
